syntax = "proto3";
option optimize_for = LITE_RUNTIME;

enum CloudMessageType {
    NORMAL = 0;
    DEVICETWIN = 1;
}

message CloudMessage {
    CloudMessageType type = 1;          // message type (from MessageType)
    string contenttype = 10;            // mime-type of the message
    map<string, string> headers = 20;   // optional custom message headers
    enum CloudMessageFlags {
        NONE = 0x0;
        NOMOBILE = 0x2;
    }
    uint32 flags = 30;
    bytes data = 40;
}

message FileReference {
    string path = 1;
}

message FileInline {
    bytes data = 1;
}


message CloudBlob {
    string destination = 10;            // relative path in blob storage to where to put the file
    enum CloudBlobFlags {
        NONE = 0x0;
        DELETEONSEND = 0x1;             // delete the file on successful transfer
        NOMOBILE = 0x2;
    }
    uint32 flags = 20;                  // bit-or combination of CloudBlobFlags
    oneof BlobSourceType {
        FileReference ref = 40;         // reference to existing file on disk
        FileInline inline = 50;         // inline file data
    }
}

message CloudDataUpload {
    enum CloudPriority {
        NONE = 0;                       // if priority is not set, NORMAL will be used
        LOWEST = 500;                   // unimportant messages that do not have to be delivered at all (will be deleted first if box is out-of-space)
        LOW = 1000;                     // low priority messages like statistics data
        NORMAL = 1500;                  // default priority when NONE is set
        HIGH = 2000;                    // high priority non-realtime message
        URGENT = 2250;                  // urgent real-time data
        CRITICAL = 2500;                // used for sever error messaging, use rarely only for device critical errors
    }
    uint32 priority = 10;               // use CloudPriority levels or a custom int value less than CRITICAL
    sint32 decayrate = 20;              // amount of priority lost (or gained) in decayinterval seconds (calculated per second), set to 0 to disable decay, default 0
    uint32 decayinterval = 30;          // amount of seconds it takes to lose decayrate priority, if decayrate is 0, this value is ignored
    uint32 minpriority = 40;            // minimum priority a message can get to through decay, default LOWEST
    uint32 maxpriority = 50;            // maximum priority a message can get to through negative decay, default CRITICAL
    oneof UploadType {
        CloudMessage msg = 60;
        CloudBlob file = 70;
    }
}

message Cloud2DeviceMessage {
    uint64 timestamp = 1;               // timestamp in microseconds
    CloudMessageType type = 10;
    string contenttype = 20;
    map<string, string> headers = 30;   // optional custom message headers
    enum Cloud2DeviceFlags {
        NONE = 0x0;
        PARTIALTWINUPDATE = 0x1;        // can be set for type DEVICETWIN
    }
    uint32 flags = 40;                  // optional message flags
    bytes data = 50;
}
